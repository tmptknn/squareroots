const Generator = (aa, bb, cc, dd) => {
    let a = aa;
    let b = bb;
    let c = cc;
    let d = dd;
    return () => {
        a >>>= 0;
        b >>>= 0;
        c >>>= 0;
        d >>>= 0;
        let t = (a + b) | 0;
        a = b ^ (b >>> 9);
        b = (c + (c << 3)) | 0;
        c = (c << 21) | (c >>> 11);
        d = (d + 1) | 0;
        t = (t + d) | 0;
        c = (c + t) | 0;
        return (t >>> 0) / 4294967296;
    };
};

const RandomGenerator = (seed) => {
    return Generator(seed, 80363, 40609, 5085083);
};

const Random = Generator(29173, 80363, 40609, 5085083);

export { RandomGenerator, Random };
