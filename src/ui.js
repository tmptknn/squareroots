import * as THREE from 'three';import { FontLoader } from 'three/addons/loaders/FontLoader.js';
import { TextGeometry } from 'three/addons/geometries/TextGeometry.js';
import { Euler, Vector3 } from 'three';

let welldone;
let help;

const loadFont = (scene) => {
    const loader = new FontLoader();
    loader.load('./arial.json', function (font) {
        const textMaterial = new THREE.MeshPhongMaterial({ color: 0xffffff });
        const geometry = new TextGeometry('Well done.\n Press x or enter for next Level', {
            font: font,
            size: 0.5,
            height: 0.1,
            curveSegments: 3,
            bevelEnabled: false,
            bevelThickness: 0.001,
            bevelSize: 0.001,
            bevelOffset: 0,
            bevelSegments: 2,
        });

        const mesh = new THREE.Mesh(geometry, textMaterial);
        mesh.position.set(-3, 1.5, 0);
        welldone = new THREE.Object3D();
        welldone.add(mesh);
        welldone.position.set(0, 0, -5);
        welldone.visible = false;
        scene.add(welldone);

        const geometryHelp = new TextGeometry(
            'SquareRoots\n a for rotate selected root(red) left\n d for rotate selected root right\n w select upper root\n s select lower root\n q select other branch\n e select other branch \n h toggle help\n in vr left stick choose root\n right stick to rotate root',
            {
                font: font,
                size: 0.25,
                height: 0.01,
                curveSegments: 3,
                bevelEnabled: false,
                bevelThickness: 0.001,
                bevelSize: 0.001,
                bevelOffset: 0,
                bevelSegments: 2,
            }
        );
        const meshHelp = new THREE.Mesh(geometryHelp, textMaterial);
        meshHelp.position.set(-3, 3, 0);
        help = new THREE.Object3D();
        help.add(meshHelp);
        help.position.set(0, 0, -5);
        scene.add(help);
    });
};

const initUI = (scene) => {
    loadFont(scene);
};

const setWon = () => {
    welldone.visible = true;
};

const resetUI = () => {
    welldone.visible = false;
};

const isWon = () => {
    return welldone.visible;
};

const isHelp = () => {
    return help.visible;
};

const showHelp = () => {
    help.visible = true;
};

const closeHelp = () => {
    help.visible = false;
};

export { initUI, setWon, resetUI, isWon, isHelp, showHelp, closeHelp };
