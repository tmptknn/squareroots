import * as THREE from 'three';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
import { VRButton } from 'three/addons/webxr/VRButton.js';
import { XRControllerModelFactory } from 'three/examples/jsm/webxr/XRControllerModelFactory.js';
import { RGBELoader } from 'three/addons/loaders/RGBELoader.js';
import { MeshPhongMaterial } from 'three';
import { Tree } from './root.js';
import { initUI, resetUI, setWon, isWon, showHelp, isHelp, closeHelp } from './ui.js';
const scene = new THREE.Scene();

const loader = new THREE.TextureLoader();

// load a resource
loader.load(
    '../pics/metsä4.jpg',

    // onLoad callback
    function (texture) {
        // in this example we create the material when the texture is loaded
        const material = new THREE.MeshBasicMaterial({
            map: texture,
        });
        texture.mapping = THREE.EquirectangularReflectionMapping;
        scene.background = texture;
    },

    // onProgress callback currently not supported
    undefined,

    // onError callback
    function (err) {
        console.error('An error happened.');
    }
);
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap

document.body.appendChild(VRButton.createButton(renderer));
renderer.xr.enabled = true;

renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
const color = 0xffffff;
const intensity = 1;
const alight = new THREE.AmbientLight(0x404040); // soft white light
scene.add(alight);
const light = new THREE.DirectionalLight(color, intensity);
light.position.set(-3, 4, 4);
light.castShadow = true; // default false
light.shadow.mapSize.width = 1024; // default
light.shadow.mapSize.height = 1024; // default
light.shadow.camera.near = 1; // default
light.shadow.camera.far = 10; // default
scene.add(light);

const levelObject = new THREE.Object3D();
levelObject.position.z = -1;
scene.add(levelObject);
const front = new THREE.Object3D();
front.position.z = 1;
const right = new THREE.Object3D();
right.position.x = 1;
levelObject.add(front);
levelObject.add(right);
let currentLevel = 0;
const levelData = [
    [0.6, 1, 8438975],
    [0.2, 3, 12345],
    [0.3, 3, 56789],
    [0.4, 4, 11223344],
    [0.5, 5, 987654321],
];

let tree = new Tree(levelData[currentLevel][0], levelData[currentLevel][1], levelData[currentLevel][2], scene);
const ui = initUI(scene);

camera.position.z = 0;
camera.position.y = 2.5;

const controls = new OrbitControls(camera, renderer.domElement);
controls.target.set(0, 1.5, levelObject.position.z);
controls.update();
controls.enablePan = false;
controls.enableDamping = true;

let tick = false;
let controllers = [];
renderer.setAnimationLoop(function () {
    for (let i = 0; i < controllers.length; i++) {
        readGamepad(controllers[i]);
    }
    renderer.render(scene, camera);
});

function actualMove(x, y, z) {
    console.log('Actual move x = ' + x + ', y = ' + y + ', z = ' + z);

    return;
}

function move(x, y, z) {
    console.log('x = ' + x + ', y = ' + y + ', z = ' + z);
    const levelOffset = new THREE.Vector3(0, 0, -1);
    const camerafront = levelOffset.sub(camera.position);
    const dotfront = levelObject.localToWorld(new THREE.Vector3(0, 0, 1)).sub(levelOffset).dot(camerafront);
    const dotright = levelObject.localToWorld(new THREE.Vector3(-1, 0, 0)).sub(levelOffset).dot(camerafront);
    const dotback = levelObject.localToWorld(new THREE.Vector3(0, 0, -1)).sub(levelOffset).dot(camerafront);
    const dotleft = levelObject.localToWorld(new THREE.Vector3(1, 0, 0)).sub(levelOffset).dot(camerafront);
    console.log(dotfront);
    let direction = 0; //front
    let dotcurrent = dotfront;
    if (dotcurrent < dotback) {
        direction = 2; //back
        dotcurrent = dotback;
    }
    if (dotcurrent < dotright) {
        direction = 1; //right
        dotcurrent = dotright;
    }
    if (dotcurrent < dotleft) {
        direction = 3; //back
        dotcurrent = dotleft;
    }
    console.log('direction ' + direction);
    switch (direction) {
        case 0:
            actualMove(x, y, z);
            break;
        case 1:
            actualMove(-z, y, x);
            break;
        case 2:
            actualMove(-x, y, -z);
            break;
        case 3:
            actualMove(z, y, -x);
            break;

        default:
            actualMove(x, y, z);
            break;
    }
}

function buildControllers() {
    const controllerModelFactory = new XRControllerModelFactory();

    const geometry = new THREE.BufferGeometry().setFromPoints([
        new THREE.Vector3(0, 0, 0),
        new THREE.Vector3(0, 0, -1),
    ]);

    const line = new THREE.Line(geometry);
    line.scale.z = 10;

    const controllers = [];

    for (let i = 0; i < 2; i++) {
        const controller = renderer.xr.getController(i);
        if (!controller) return;
        //controller.add(line.clone());
        controller.userData.selectPressed = false;
        controller.userData.selectPressedPrev = false;
        scene.add(controller);
        controllers.push(controller);

        const grip = renderer.xr.getControllerGrip(i);
        grip.add(controllerModelFactory.createControllerModel(grip));
        scene.add(grip);
        if (controller) {
            controller.userData.id = 0;
            scene.add(controller);

            controller.addEventListener('connected', (e) => {
                controller.gamepad = e.data.gamepad;
                controller.handedness = e.data.handedness;
            });
        }
        controllers[i] = controller;
    }

    return controllers;
}
controllers = buildControllers();
/*
controller2 = renderer.xr.getController(1);
//controller2.addEventListener('selectstart', onSelectStart);
//controller2.addEventListener('selectend', onSelectEnd);
controller2.userData.id = 1;
scene.add(controller2);
*/

function doNotRepeat() {
    let lastRun = performance.now() / 1000;
    return (fun) => {
        const currentRun = performance.now() / 1000;
        if (lastRun + 1.2 < currentRun) {
            lastRun = currentRun;
            fun();
        }
    };
}

const runOnce = doNotRepeat();

const nextLevel = () => {
    runOnce(() => {
        if (isHelp()) {
            closeHelp();
        }
        if (isWon()) {
            resetUI();
            tree.cleanUp();
            scene.remove(tree.object());
            currentLevel += 1;
            if (currentLevel < levelData.length) {
                tree = tree = new Tree(
                    levelData[currentLevel][0],
                    levelData[currentLevel][1],
                    levelData[currentLevel][2],
                    scene
                );
            } else {
                tree = new Tree(0.6, 5, Math.floor(Math.random() * 123456), scene);
            }
        }
    });
};

const turnLeft = () => {
    runOnce(() => {
        tree.rotateCurrentLeft(() => {
            solvedAudio.play();
            if (isHelp()) closeHelp();
            setWon();
        });
        swifAudiio.pause();
        swifAudiio.currentTime = 0;
        swifAudiio.play();
    });
};

const turnRight = () => {
    runOnce(() => {
        tree.rotateCurrentRight(() => {
            solvedAudio.play();
            if (isHelp()) closeHelp();
            setWon();
        });
        swifAudiio.pause();
        swifAudiio.currentTime = 0;
        swifAudiio.play();
    });
};

const solvedAudio = new Audio('./puzzlesolved.mp3');
const swifAudiio = new Audio('./CasualGameSFXPackswish.mp3');
function readGamepad(controller) {
    if (controller && controller.gamepad) {
        switch (
            controller.handedness //or is it controller.gamepad.handedness
        ) {
            case 'none':
                break;
            case 'left':
                //touchpad
                if (controller.gamepad.axes[1] < -0.5) {
                }
                if (controller.gamepad.axes[1] > 0.5) {
                }
                if (controller.gamepad.axes[0] < -0.5) {
                }
                if (controller.gamepad.axes[0] > 0.5) {
                }
                if (controller.gamepad.buttons[2].pressed) {
                    //touchpad press
                }

                //stick
                if (controller.gamepad.axes[3] < -0.5) {
                    runOnce(() => {
                        tree.selectUpperRoot();
                    });
                }
                if (controller.gamepad.axes[3] > 0.5) {
                    runOnce(() => {
                        tree.selectLowerRoot();
                    });
                }
                if (controller.gamepad.axes[2] < -0.5) {
                    //levelObject.rotation.y += 0.01 * controller.gamepad.axes[2];
                    runOnce(() => {
                        tree.selectPreviousRoot();
                    });
                }
                if (controller.gamepad.axes[2] > 0.5) {
                    //levelObject.rotation.y += 0.01 * controller.gamepad.axes[2];
                    runOnce(() => {
                        tree.selectNextRoot();
                    });
                }

                if (controller.gamepad.buttons[3].pressed) {
                    //stick press
                }

                if (controller.gamepad.buttons) {
                    if (controller.gamepad.buttons[0].pressed) {
                        //trigger
                    }

                    if (controller.gamepad.buttons[1].pressed) {
                        //squeeze
                    }
                    if (controller.gamepad.buttons[5].pressed) {
                        //B

                        runOnce(() => {
                            tree.shuffle();
                        });
                    }
                    if (controller.gamepad.buttons[4].pressed) {
                        //A
                        nextLevel();
                    }
                }
                break;
            case 'right':
                if (tick) return;
                //touchpad
                if (controller.gamepad.axes[1] < -0.5) {
                    //move(0, 0, 1);
                }
                if (controller.gamepad.axes[1] > 0.5) {
                    //move(0, 0, -1);
                }
                if (controller.gamepad.axes[0] < -0.5) {
                    turnLeft();
                }
                if (controller.gamepad.axes[0] > 0.5) {
                    turnRight();
                }
                if (controller.gamepad.buttons[2].pressed) {
                    //touchpad press
                    //move(0, -1, 0);
                }

                //stick
                if (controller.gamepad.axes[3] < -0.5) {
                    //move(0, 0, 1);
                }
                if (controller.gamepad.axes[3] > 0.5) {
                    //move(0, 0, -1);
                }
                if (controller.gamepad.axes[2] < -0.5) {
                    turnLeft();
                }
                if (controller.gamepad.axes[2] > 0.5) {
                    turnRight();
                }

                if (controller.gamepad.buttons[3].pressed) {
                    //stick press
                    //move(0, -1, 0);
                }

                if (controller.gamepad.buttons) {
                    if (controller.gamepad.buttons[0].pressed) {
                        //trigger
                        //move(0, 1, 0);
                    }

                    if (controller.gamepad.buttons[1].pressed) {
                        //squeeze
                        //move(0, -1, 0); //this should be undo
                    }
                    if (controller.gamepad.buttons[5].pressed) {
                        //B
                        //move(0, 1, 0); //maybe menu
                        runOnce(() => {
                            tree.solve();
                        });
                    }
                    if (controller.gamepad.buttons[4].pressed) {
                        //A
                        runOnce(() => {
                            if (isHelp()) {
                                closeHelp();
                                audio.play();
                            } else showHelp();
                        });
                        //move(0, -1, 0); // undo again? or restart
                        //tree.
                    }
                }
                break;
            default:
                break;
        }
    }
}

const audio = new Audio('./lyricandrelaxingbackgroundmusicpack.mp3');

audio.loop = true;

document.onkeydown = function (e) {
    e.preventDefault();
    switch (e.code) {
        case 'Numpad5':
            console.log('Go Up ');
            move(0, 1, 0);
            break;
        case 'Numpad8':
            console.log('Go Forward ');
            move(0, 0, 1);
            break;
        case 'Numpad4':
            console.log('Go Left ');
            move(1, 0, 0);
            break;
        case 'Numpad6':
            console.log('Go Right ');
            move(-1, 0, 0);
            break;
        case 'Numpad2':
            console.log('Go Backward ');
            move(0, 0, -1);
            break;
        case 'Numpad0':
            console.log('Go Down ');
            move(0, -1, 0);
            break;
        case 'ArrowLeft':
            levelObject.rotation.y -= 0.1;
            break;
        case 'ArrowRight':
            levelObject.rotation.y += 0.1;
            break;
        case 'KeyW':
            tree.selectUpperRoot();
            break;
        case 'KeyS':
            tree.selectLowerRoot();
            break;
        case 'KeyE':
            tree.selectNextRoot();
            break;
        case 'KeyQ':
            tree.selectPreviousRoot();
            break;
        case 'KeyA':
            turnRight();
            break;
        case 'KeyD':
            turnLeft();
            break;

        case 'KeyS':
            tree.shuffle();
            break;
        case 'KeyX':
            tree.solve();
            break;
        case 'KeyH':
            if (isHelp()) {
                closeHelp();
                audio.play();
            } else showHelp();
            break;
        case 'Enter':
            nextLevel();
            break;

        default:
            console.log(e);
            break;
    }
};

document.onkeyup = function (e) {
    e.preventDefault();
    switch (e.code) {
        default:
            console.log(e);
            break;
    }
};
