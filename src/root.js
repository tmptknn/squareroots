import * as THREE from 'three';
import { Euler, Vector3 } from 'three';
import { Random, RandomGenerator } from './random.js';

export function easeInOutQuad(t) {
    return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
}

class Root {
    constructor(parent, size, position, rotations) {
        this.lastAngle = 0;
        this.parent = parent;
        this.size = size;
        this.rotations = rotations;
        this.children = [];
        this.joints = [];
        this.position = position;
        this.rotation = 0;
        this.lastRotation = 0;
        this.startRotation = 0;
        this.rootSelectedMaterial = new THREE.MeshPhongMaterial({ color: 0xbb0000 });
        this.rootCurrentMaterial = this.rootMaterial = new THREE.MeshPhongMaterial({ color: 0xffaa11 });
        this.rootOkMaterial = new THREE.MeshPhongMaterial({ color: 0x11ff11 });
        this.makeGeometry(size);
        this.lastSelectedChild = null;
    }

    shuffle() {
        //this.startRotation = this.lastRotation;
        this.lastRotation = this.rotation;
        this.rotation = Math.floor(Math.random() * this.rotations);
        this.applyRotation();
    }

    addChild(child, rot) {
        this.children.push({ child, rotation: rot });
    }

    isLeaf() {
        return this.children.length === 0;
    }

    object() {
        return this.object3d;
    }

    applyRecursively(obj, mat) {
        if (obj.type == 'Mesh') {
            obj.material = mat;
        }
        obj.children.forEach((child) => this.applyRecursively(child, mat));
    }

    applyMaterial(material) {
        this.mesh.material = material;
        if (this.joints.length > 0) {
            for (let i = 0; i < this.joints.length; i++) {
                this.applyRecursively(this.joints[i], material);
            }
        }
    }

    select() {
        this.applyMaterial(this.rootSelectedMaterial);
    }

    unselect() {
        this.applyMaterial(this.currentRootMaterial);
    }
    setOk() {
        this.applyMaterial((this.currentRootMaterial = this.rootOkMaterial));
    }

    isOk() {
        return this.currentRootMaterial == this.rootOkMaterial;
    }

    setNotOk() {
        this.applyMaterial((this.currentRootMaterial = this.rootMaterial));
    }

    addJoint(joint) {
        this.joints.push(joint);
    }

    makeGeometry(size) {
        this.object3d = new THREE.Object3D();
        this.object3d.position.set(this.position[0], this.position[1], this.position[2]);
        const coneGeometry = new THREE.ConeGeometry(size[0], size[1], 21);

        this.mesh = new THREE.Mesh(coneGeometry, this.rootMaterial);
        this.mesh.castShadow = true;
        this.mesh.rotateOnAxis(new THREE.Vector3(1, 0, 0), Math.PI);
        this.mesh.position.set(0, -size[1] / 2, 0);
        this.object3d.add(this.mesh);
        if (this.parent !== null) {
            this.parent.object().add(this.object());
        }
    }

    getLastSelectedChild() {
        if (this.lastSelectedChild !== null) {
            return this.lastSelectedChild;
        }
        for (let i = 0; i < this.children.length; i++) {
            if (!this.children[i].child.isLeaf()) {
                return (this.lastSelectedChild = this.children[i].child);
            }
        }
        return null;
    }

    getNextChild() {
        if (this.lastSelectedChild !== null) {
            for (let i = 0; i < this.children.length; i++) {
                if (this.lastSelectedChild === this.children[i].child) {
                    for (let j = 0; j < this.children.length; j++) {
                        const index = (i + j + 1) % this.children.length;
                        if (!this.children[index].child.isLeaf()) {
                            this.lastSelectedChild = this.children[index].child;
                            return this.lastSelectedChild;
                        }
                    }
                    return null;
                }
            }
            return null;
        }
        for (let i = 0; i < this.children.length; i++) {
            if (!this.children[i].child.isLeaf()) {
                return (this.lastSelectedChild = this.children[i].child);
            }
        }
        return null;
    }

    getPreviousChild() {
        if (this.lastSelectedChild !== null) {
            for (let i = 0; i < this.children.length; i++) {
                if (this.lastSelectedChild === this.children[i].child) {
                    for (let j = 0; j < this.children.length; j++) {
                        const index = (i - 1 - j + 2 * this.children.length) % this.children.length;
                        if (!this.children[index].child.isLeaf()) {
                            return (this.lastSelectedChild = this.children[index].child);
                        }
                    }
                    return null;
                }
            }
            return null;
        }
        for (let i = this.children.length - 1; i >= 0; i--) {
            if (!this.children[i].child.isLeaf()) {
                return (this.lastSelectedChild = this.children[i].child);
            }
        }
        return null;
    }
    applyRotation() {
        this.object().setRotationFromEuler(new THREE.Euler(0, (this.rotation * Math.PI * 2) / this.rotations, 0));
    }

    solve(callback) {
        if (this.rotation != this.startRotation) {
            this.lastRotation = this.rotation;
            if (
                Math.abs((this.rotation - this.startRotation + this.rotations) % this.rotations) <
                Math.abs((this.startRotation - this.rotation + this.rotations) % this.rotations)
            ) {
                this.rotation = (this.rotation - 1 + this.rotations) % this.rotations;
            } else {
                this.rotation = (this.rotation + 1) % this.rotations;
            }
            this.correctRotation(() => {
                this.solve(callback);
                callback();
            });
        }
    }

    correctRotation(callback) {
        if (this.rotationInApply) return;
        const delay = 1.2;
        const currentAngle = (this.rotation * Math.PI * 2) / this.rotations;
        const lastTime = performance.now() / 1000;
        this.rotationInApply = setInterval(() => {
            if (lastTime + delay < performance.now() / 1000) {
                clearInterval(this.rotationInApply);
                this.rotationInApply = null;
                this.object().setRotationFromEuler(new THREE.Euler(0, currentAngle, 0));
                if (callback) callback();
                return;
            }
            const delta = easeInOutQuad((performance.now() / 1000 - lastTime) / delay);
            const lastAngle = (this.lastRotation * Math.PI * 2) / this.rotations;

            const diff = Math.min(
                Math.abs((currentAngle - lastAngle + Math.PI * 2) % (Math.PI * 2)),
                Math.abs((lastAngle - currentAngle + Math.PI * 2) % (Math.PI * 2))
            );
            const rotationdirection = (this.rotation - this.lastRotation + this.rotations) % this.rotations <= 1;

            if (rotationdirection) {
                this.object().setRotationFromEuler(new THREE.Euler(0, lastAngle + delta * diff, 0));
            } else {
                this.object().setRotationFromEuler(new THREE.Euler(0, lastAngle - delta * diff, 0));
            }
        }, 15);
    }

    rotateRight(callback) {
        if (this.rotationInApply) return;
        this.lastRotation = this.rotation;
        this.rotation = (this.rotation + 1) % this.rotations;
        this.correctRotation(callback);
    }

    rotateLeft(callback) {
        if (this.rotationInApply) return;
        this.lastRotation = this.rotation;
        this.rotation = (this.rotation - 1 + this.rotations) % this.rotations;
        this.correctRotation(callback);
    }
}

class Tree {
    constructor(complexity, depth, seed, scene) {
        this.complexity = complexity;
        this.depth = depth;
        this.seed = seed;
        this.scene = scene;
        this.object3d = new THREE.Object3D();
        this.random = RandomGenerator(seed);
        this.roots = [];
        this.targetObjects = [];
        this.targetRadius = 0.025;
        //
        this.object3d.position.x = 0;
        this.object3d.position.y = 2;
        this.object3d.position.z = -1;
        this.object3d.setRotationFromEuler(new THREE.Euler(0, Math.PI / 4, 0));
        this.generateRoots(this.depth);

        this.createTargets();
        this.shuffle();
    }
    object() {
        return this.object3d;
    }

    addRoot(current, size, position, rotations, depth) {
        const root = new Root(current, size, position, rotations);
        const scale = 0.5;
        const separation = 1;
        if (current !== null) {
            current.addChild(root);
        }
        this.roots.push(root);
        if (depth > 0) {
            /*
            const r2 = this.random();
            if(r2<this.complexity){
                
                this.addRoot(root,childsize,childPosition,rotations, depth -1);
            }*/
            for (let i = 0; i < rotations; i++) {
                const r = this.random();
                //console.log(r);
                if (r < this.complexity) {
                    const a = (i * Math.PI * 2) / rotations;
                    const childsize = [size[0] * scale, size[1] * 0.5, size[2] / 3];
                    const childPosition = [
                        Math.sin(a) * separation * childsize[2],
                        -size[1] * 0.5,
                        Math.cos(a) * separation * childsize[2],
                    ];
                    const middlePoint = [
                        childPosition[0] / 2,
                        childPosition[1] / 2 + size[1] / 14,
                        childPosition[2] / 2,
                    ];
                    const len = Math.sqrt(
                        middlePoint[0] * middlePoint[0] +
                            middlePoint[1] * middlePoint[1] +
                            middlePoint[2] * middlePoint[2]
                    );

                    const diffPosition = [
                        -middlePoint[0] + childPosition[0],
                        -middlePoint[1] + childPosition[1],
                        -middlePoint[2] + childPosition[2],
                    ];
                    const len2 = Math.sqrt(
                        diffPosition[0] * diffPosition[0] +
                            diffPosition[1] * diffPosition[1] +
                            diffPosition[2] * diffPosition[2]
                    );

                    const jointobject = new THREE.Object3D();
                    const jointMaterial = new THREE.MeshPhongMaterial({ color: 0xffaa11 });
                    const joingeometry = new THREE.CylinderGeometry(size[0], size[0] * 0.75 + childsize[0] * 0.25, len);
                    const jointmesh = new THREE.Mesh(joingeometry, jointMaterial);
                    jointmesh.position.set(0, -len / 2, 0);
                    jointobject.add(jointmesh);

                    const joingeometry3 = new THREE.CylinderGeometry(
                        size[0] * 0.75 + childsize[0] * 0.25,
                        childsize[0],
                        len2
                    );
                    const jointmesh3 = new THREE.Mesh(joingeometry3, jointMaterial);
                    const joingeometry4 = new THREE.SphereGeometry(size[0] * 0.75 + childsize[0] * 0.25);
                    const jointmesh4 = new THREE.Mesh(joingeometry4, jointMaterial);
                    const jointObject3 = new THREE.Object3D();
                    jointObject3.add(jointmesh4);
                    jointObject3.position.set(0, -len, 0);
                    jointmesh3.position.set(0, -len2 / 2, 0);
                    jointObject3.rotateX(
                        Math.atan2(
                            Math.sqrt(diffPosition[0] * diffPosition[0] + diffPosition[2] * diffPosition[2]),
                            diffPosition[1]
                        ) -
                            Math.atan2(
                                Math.sqrt(middlePoint[0] * middlePoint[0] + middlePoint[2] * middlePoint[2]),
                                middlePoint[1]
                            )
                    );
                    jointObject3.add(jointmesh3);
                    jointobject.add(jointObject3);
                    root.addJoint(jointobject);
                    jointobject.rotateY(Math.atan2(childPosition[0], childPosition[2]));
                    jointobject.rotateX(
                        Math.PI +
                            Math.atan2(
                                Math.sqrt(middlePoint[0] * middlePoint[0] + middlePoint[2] * middlePoint[2]),
                                middlePoint[1]
                            )
                    );

                    const joint2geometry = new THREE.SphereGeometry(childsize[0]);

                    const joint2mesh = new THREE.Mesh(joint2geometry, jointMaterial);
                    joint2mesh.position.set(childPosition[0], childPosition[1], childPosition[2]);
                    root.object().add(jointobject);
                    root.object().add(joint2mesh);
                    this.addRoot(root, childsize, childPosition, rotations, depth - 1);
                }
            }
        }
        return root;
    }

    innerSelectRoot(root) {
        if (root === null) return;
        this.selectedRoot.unselect();
        this.selectedRoot = root;
        this.selectedRoot.select();
    }

    selectLowerRoot() {
        const current = this.selectedRoot;

        if (!current.isLeaf()) {
            const lastChild = current.getLastSelectedChild();

            this.innerSelectRoot(lastChild);
        }
    }

    selectUpperRoot() {
        const current = this.selectedRoot;
        if (current.parent) {
            this.innerSelectRoot(current.parent);
        }
    }

    selectNextRoot() {
        const current = this.selectedRoot;
        if (current.parent) {
            this.innerSelectRoot(current.parent.getNextChild());
        }
    }

    selectPreviousRoot() {
        const current = this.selectedRoot;
        if (current.parent) {
            this.innerSelectRoot(current.parent.getPreviousChild());
        }
    }

    generateRoots(depth) {
        const rotations = 6;
        this.mainRoot = this.addRoot(null, [0.03, 1.0, 2], [0, 0, 0], rotations, Math.min(depth, 3));
        this.selectedRoot = this.mainRoot;
        this.selectedRoot.select();
        this.object3d.add(this.mainRoot.object());
        this.scene.add(this.object());
    }

    rotateCurrentLeft(callback) {
        this.selectedRoot.rotateLeft(() => {
            if (this.checkTargets()) {
                callback();
            }
        });
    }

    rotateCurrentRight(callback) {
        this.selectedRoot.rotateRight(() => {
            if (this.checkTargets()) {
                callback();
            }
        });
    }

    shuffle() {
        for (let i = 0; i < this.roots.length; i++) {
            const root = this.roots[i];
            if (!root.isLeaf()) {
                root.shuffle();
            }
        }
        if (this.checkTargets()) this.shuffle();
    }

    createTargets() {
        for (let i = 0; i < this.roots.length; i++) {
            const root = this.roots[i];
            const position = root.object().getWorldPosition(new Vector3(0, 0, 0));
            position.add(new Vector3(0, -root.size[1], 0));
            const targetMaterial = new THREE.MeshPhongMaterial({ color: 0xaaaaaa });
            const targetGeometry = new THREE.SphereGeometry(this.targetRadius);
            const targetMesh = new THREE.Mesh(targetGeometry, targetMaterial);
            const targetObject = new THREE.Object3D();
            targetObject.position.set(position.x, position.y, position.z);
            targetObject.add(targetMesh);
            this.scene.add(targetObject);
            this.targetObjects.push(targetObject);
        }
        this.checkTargets();
    }

    checkTargets() {
        let notAllFound = false;
        for (let i = this.roots.length - 1; i >= 0; i--) {
            const root = this.roots[i];
            const position = root.object().getWorldPosition(new Vector3(0, 0, 0));
            position.add(new Vector3(0, -root.size[1], 0));
            let found = false;

            for (let j = 0; j < this.targetObjects.length; j++) {
                const target = this.targetObjects[j];
                const targetPosition = target.getWorldPosition(new Vector3(0, 0, 0));

                if (position.distanceTo(targetPosition) < this.targetRadius) {
                    let childrenOk = true;
                    for (let k = 0; k < root.children.length; k++) {
                        let child = root.children[k].child;
                        if (!child.isOk()) {
                            childrenOk = false;
                        }
                    }
                    if (childrenOk) {
                        root.setOk();
                        if (root == this.selectedRoot) {
                            root.select();
                        }
                        found = true;
                    }
                }
            }
            if (!found) {
                notAllFound = true;
                root.setNotOk();
                if (root == this.selectedRoot) {
                    root.select();
                }
            }
        }
        if (notAllFound) {
            console.log('Found root with no target');
            return false;
        }
        console.log('All roots in targets');
        return true;
    }
    cleanUp() {
        for (let i = 0; i < this.targetObjects.length; i++) {
            this.scene.remove(this.targetObjects[i]);
        }
        this.scene.remove(this.object());
    }

    solve() {
        for (let i = 0; i < this.roots.length; i++) {
            const root = this.roots[i];
            if (!root.isLeaf()) {
                root.solve(() => {
                    this.checkTargets();
                });
            }
        }
    }
}
export { Tree };
